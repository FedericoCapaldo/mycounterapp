package com.federicocapaldo.mycounterapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    int MAX_VALUE = 100;
    int value = 0;
    //to be reviewed after the tutorial. how to assign view to object. Page 17 point 3.
    TextView textView ;
    SeekBar seekbar ;
    Button increaseButton ;
    Button decreaseButton ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initialise various components
        textView = (TextView) findViewById(R.id.myView);
        seekbar = (SeekBar) findViewById(R.id.seekBar);
        increaseButton = (Button) findViewById(R.id.button2);
        decreaseButton = (Button) findViewById(R.id.button);

        seekbar.setMax(MAX_VALUE);

        seekbar.setOnSeekBarChangeListener(this);

        displayValue();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void increaseValue(View view) {
        if(value >= MAX_VALUE) {
            value = 0;
        } else {
            value += 1;
        }
        displayValue();
    }

    public void decreaseValue(View view) {
        if(value <= 0) {
            value = MAX_VALUE;
        } else {
            value -=1 ;
        }
        displayValue();
    }

    //called on Create
    public void displayValue() {
        textView.setText(Integer.toString(value));
        seekbar.setProgress(value);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        value = progress;
        displayValue();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
